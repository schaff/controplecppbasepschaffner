#ifndef ROULETTE_H
#define ROULETTE_H

#include <iostream>
#include <stdlib.h>

#include "joueur.h"


class roulette
{
    public:
        roulette();
        virtual ~roulette();

        bool lancerPartieRoulette();
        void choisir();
        bool gagne();

    protected:

    private:
        int choixJoueur;
        bool partieGagne;
        bool win;
};

#endif // ROULETTE_H

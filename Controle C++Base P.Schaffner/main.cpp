#include <iostream>
#include <stdlib.h>
#include <time.h>

#include "casino.h"

using namespace std;

int main()
{
    //Initialiser le random
    srand(time(NULL));

    //constructeur
    casino *nouveauClient = new casino();

    //Lancement du jeu
    nouveauClient->startGame();

    //destructeur
    delete nouveauClient;
    return 0;
}

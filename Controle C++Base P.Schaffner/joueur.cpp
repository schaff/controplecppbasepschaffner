#include "joueur.h"

joueur::joueur()
{
    //ctor
    nbJetons= 10;
    compteurGagn=0;
}

joueur::~joueur()
{
    //dtor
}
void joueur::setCompteurGagne(int nbGagne)
{
    compteurGagn=nbGagne;
}
joueur::getCompteurGagne()
{
    return compteurGagn;
}

void joueur::setMise(int mise)
{
    this->mise = mise;
}
joueur::getMise()
{
    return mise;
}

void joueur::setNbJetons(int nbJetons)
{
    this->nbJetons = nbJetons;
}
joueur::getNbJetons()
{
    return nbJetons;
}

void joueur::setPv(int pv)
{
    this->pv = pv;
}
joueur::getPv()
{
    return pv;
}

void joueur::choixDeLaMise() //fonction qui permet de miser pour chaque jeu
{
    std::cout<<"VOUS AVEZ : "<<getNbJetons()<<" JETONS"<<std::endl;
    std::cout<<"CHOISISSEZ VOTRE MISE : ";
    int choixMise;
    std::cin>>choixMise;
    setMise(choixMise);
    while (choixMise>getNbJetons()) // controle de la mise / on peut pas miser plus de jeton qu'on poss�de
    {
        std::cout<<"VOTRE MISE EST TROP IMPORTANTE !! VOUS AVEZ UNIQUEMENT "<<getNbJetons()<<" JETONS"<<std::endl;
        std::cout<<"CHOISISSEZ VOTRE MISE : ";
        std::cin>>choixMise;
        setMise(choixMise);
    }
}

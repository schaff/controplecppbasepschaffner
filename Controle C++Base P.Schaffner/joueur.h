#ifndef JOUEUR_H
#define JOUEUR_H

#include <iostream>
#include <stdlib.h>


class joueur
{
    public:
        joueur();
        virtual ~joueur();

        void choixDeLaMise();

        void setMise(int mise);
        int getMise();

        void setNbJetons(int nbJetons);
        int getNbJetons();

        void setPv(int pv);
        int getPv();

        void setCompteurGagne(int nbGagne);
        int getCompteurGagne();


    protected:

    private:
        int nbJetons;
        int mise;
        int choix;
        int pv;
        int compteurGagn;
};

#endif // JOUEUR_H

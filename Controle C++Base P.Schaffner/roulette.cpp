#include "roulette.h"

roulette::roulette()
{
    //ctor
    partieGagne=false;
}

roulette::~roulette()
{
    //dtor
}

void roulette::choisir() //permet de choisir sur quoi on va miser
{
    std::cout << "QUEL EST VOTRE CHOIX ? (0)paire / (1)impaire " ;
    std::cin >> choixJoueur;
    while(choixJoueur!=0 && choixJoueur!=1) //controle du choix
    {
        std::cout << "ERREUR, QUEL EST VOTRE CHOIX ? (0)paire / (1)impaire "<<std::endl;
        std::cin >> this->choixJoueur;
    }
}

bool roulette::gagne() //permet de v�rifier qui a gagn�
{
    int nbHasard = rand()%2;
    if (choixJoueur==nbHasard)
    {
        std::cout << "LE JOUEUR GAGNE" << std::endl;
        win=true;
    }
    else
    {
        std::cout << "LE JOUEUR PERD" << std::endl;
        win=false;
    }
    return win;
}

bool roulette::lancerPartieRoulette() // lancement du jeu roulette
{
    choisir();
    partieGagne=gagne();

    return partieGagne;
}

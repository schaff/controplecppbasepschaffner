#include "mastermind.h"

mastermind::mastermind()
{
    //ctor
    nbEssai = 2;
    rejoue = 0;
    partieGagne = false;


}

mastermind::~mastermind()
{
    //dtor
}

mastermind::getRejoue()
{
    return rejoue;
}

void mastermind::setRejoue(int rejoue)
{
    this->rejoue = rejoue;
}

mastermind::getNbEssai()
{
    return nbEssai;
}
void mastermind::setNbEssai(int nbEssai)
{
    this->nbEssai=nbEssai;
}

void mastermind::setBienPlace(int bienPlace)
{
    this->bienPlace = bienPlace;
}
void mastermind::setMalPlace(int malPlace)
{
    this->malPlace = malPlace;
}

bool mastermind::initialisePartieGagne ()
{
    partieGagne = false;
    return partieGagne;
}

mastermind::getBienPlace()
{
    return bienPlace;
}
mastermind::getMalPlace()
{
    return malPlace;
}

bool mastermind::lancerPartie() // permet de lancer le jeu du mastermind
{
    setCombinaison();
    setNbEssai(10);
    partieGagne=false;
    std::cout<<"NOMBRE D'ESSAI TOTAL : "<<getNbEssai()<<std::endl;
    //initialisePartieGagne();
    while (partieGagne == false && nbEssai > 0) // boucle de jeu
    {
        setCombiJoueur();
        controleCombinaison();
        setNbEssai(getNbEssai()-1);
        std::cout<<"NOMBRE D'ESSAI RESTANT : "<<getNbEssai()<<std::endl;
        gagnePartie();
        setBienPlace(0);
        setMalPlace(0);
    }
    return partieGagne;
}

void mastermind::controleCombinaison () //controle des combinaison pour d�terminer qui a gagner
{
    for (int i = 0; i<4; i++)
    {
        if (combinaisonJoueur [i] == combinaison [i])
        {
            setBienPlace(getBienPlace()+1); //le chiffre existe dans les deux combinaison et est bien plac�
        }
        else
        {
            for (int j=0; j<4; ++j)
            {
                if (i!=j)
                    {
                        //On ne reteste pas le bien place
                        if (combinaisonJoueur [i] == combinaison [j])
                        {
                            setMalPlace(getMalPlace()+1); //le chiffre existe dans les combinaison mais n'est pas bien plac�
                            break; //On evite de compter plusieurs fois
                        }
                    }
            }
        }
    }
    std::cout<<"NOMBRE DE CHIFFRE BIEN PLACE : "<<getBienPlace()<<std::endl;
    std::cout<<"NOMBRE DE CHIFFRE MAL PLACE : "<<getMalPlace()<<std::endl;
}

void mastermind::gagnePartie() // permet de voir qui a gagn� la partie
{
    if (getBienPlace()==4)
    {
        partieGagne = true;
        std::cout<<"PARTIE GAGNE"<<std::endl;
    }
    else if (nbEssai < 0)
    {
        std::cout<<"PARTIE PERDU"<<std::endl;
    }
}

void mastermind::setCombiJoueur() //permet de composer la combinaison qui permet de trouver la combinaison secrete
{
    for (int i=0; i<4; i++)
    {
        int pion=0;
        std::cout<<"SELECTIONNEZ UN CHIFFRE ENTRE 0 ET 9"<<std::endl;
        std::cin>>pion;
        while (pion<0 || pion>9)
        {
            std::cout << "ERREUR, CHIFFRE INCORRECT !" << std::endl;
            std::cin >> pion;
        }
        combinaisonJoueur [i]=pion;
    }
}

void mastermind::setCombinaison() //permet de composer la combinaison secrete
{
    for (int i=0; i<4; i++)
    {
        int pion=rand()%10;
        combinaison[i]=pion;
        //std::cout<<"COMBI : "<<combinaison[i]<<std::endl;

    }
}

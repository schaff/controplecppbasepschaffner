#ifndef MASTERMIND_H
#define MASTERMIND_H

#include <iostream>
#include <stdlib.h>

#include "joueur.h"



class mastermind
{
    public:
        mastermind();
        virtual ~mastermind();

        bool lancerPartie ();
        void controleCombinaison ();
        void gagnePartie();

        int saisirEmplacement();

        int getRejoue();
        void setRejoue(int rejoue);

        void setCombiJoueur();
        void setCombinaison();


        int *getCombiJoueur();
        int *getCombinaison();

        void setBienPlace(int bienPlace);
        void setMalPlace(int malPlace);


        int getBienPlace();
        int getMalPlace();

        void gestionScore();

        void setNbEssai(int nbEssai);
        int getNbEssai ();

        bool initialisePartieGagne ();


    protected:

    private:
        int nbEssai;
        int combinaison[4];
        int combinaisonJoueur [4];
        int rejoue;
        bool partieGagne;
        int bienPlace;
        int malPlace;

};

#endif // MASTERMIND_H

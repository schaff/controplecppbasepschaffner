#ifndef PILEFACE_H
#define PILEFACE_H

#include <iostream>
#include <stdlib.h>

#include "joueur.h"


class pileFace
{
    public:
        pileFace(joueur &gamer);
        virtual ~pileFace();

        bool guerre();
        void bataille();
        void tourDeJeu();
        void lancer();
        void gagne();
        void gestionScore();

        int getOrdiPv();
        void setOrdiPv(int pv);

        int getOrdiNbGagne();
        void setOrdiNbGagne(int nbGagne);

    protected:

    private:
        joueur *face;
        int resRand;
        int ordiPv;
        int ordiNbGagne;
        bool partieGagne;
        joueur *gamer;
};

#endif // PILEFACE_H

#ifndef CASINO_H
#define CASINO_H

#include "mastermind.h"
#include "joueur.h"
#include "pileFace.h"
#include "roulette.h"

class casino
{
    public:
        casino();
        virtual ~casino();

        void startGame ();

        void setRestart (int restart);
        int getrestart();

        void gestionScore();

    protected:

    private:
        joueur *gamer;
        mastermind *master;
        pileFace *pile;
        roulette *roul;
        int restart;
        bool partieGagne;
};

#endif // CASINO_H

#include "casino.h"

casino::casino() //constructeur
{
    //ctor
    gamer = new joueur();
    master = new mastermind();
    pile = new pileFace(*gamer);
    roul = new roulette();
    restart=0;
    partieGagne=false;
}

casino::~casino() //destructeur
{
    //dtor
    delete gamer;
    delete master;
    delete pile;
    delete roul;
}

void casino::startGame() //fonction de lancement du jeu
{
    std::cout<<"BONJOUR ET BIENVENU DANS MON CASINO"<<std::endl<<"VENEZ JOUER A TOUS NOS JEUX"<<std::endl; //Message d'accueil
    while (getrestart()==0)
    {
        std::cout<<"SELECTIONNEZ VOTRE JEU : (1)Mastermind / (2)Pile ou Face / (3)Roulette"<<std::endl;
        int choix=0;
        std::cin>>choix;
        switch(choix) //permet de choisir le jeu
        {
        case 1:
            gamer->choixDeLaMise();
            partieGagne=master->lancerPartie();
            break;
        case 2:
            gamer->choixDeLaMise();
            partieGagne=pile->guerre();
            break;
        case 3:
            gamer->choixDeLaMise();
            partieGagne=roul->lancerPartieRoulette();
            break;
        }
        gestionScore();
        if (partieGagne==true){ //condition qui v�rifie les jetons / plus de jetons peu plus jouer
            std::cout<<"VOUS VOULEZ RECOMMENCER ? FAITE VOTRE CHOIX ! (0)Oui / (1)Non"<<std::endl;
            int retry;
            std::cin>>retry;
            setRestart(retry);
        }else{
            std::cout<<"TU ES PAUVRE"<<std::endl;
        break;
        }
    }
}

void casino::gestionScore() // fonction d'ajout ou de retrait de jetons
{
    if (partieGagne == true)
    {
        gamer->setNbJetons(gamer->getNbJetons()+gamer->getMise()); // ajour de jetons
    }else if (partieGagne == false)
    {
        gamer->setNbJetons(gamer->getNbJetons()-gamer->getMise()); // retrait de jeton
    }
}

void casino::setRestart(int restart)
{
    this->restart=restart;
}
casino::getrestart()
{
    return restart;
}

#include "pileFace.h"

pileFace::pileFace(joueur &gamer)
{
    //ctor
    resRand = 0;
    partieGagne=false;
    this->gamer = &gamer;
}

pileFace::~pileFace()
{
    //dtor

}

void pileFace::setOrdiNbGagne(int nbGagne)
{
    ordiNbGagne = nbGagne;
}
pileFace::getOrdiNbGagne()
{
    return ordiNbGagne;
}

void pileFace::setOrdiPv(int pv)
{
    ordiPv = pv;
}
pileFace::getOrdiPv()
{
    return ordiPv;
}

bool pileFace::guerre() // lancement du jeu pile ou face
{
    //Lancer les 100 batailles
    for (int i=0; i<10; ++i)
    {
        bataille(); // lance la m�thode bataille
    }
    if (gamer->getPv()>0) //v�rif de qui a gagn� la partie
    {
        partieGagne = true;
    }
    else
    {
        partieGagne = false;
    }

    //Resultat
    std::cout << "PILE A GAGNE " << gamer->getCompteurGagne() << " FOIS" << std::endl;
    std::cout << "FACE A GAGNE " << getOrdiNbGagne() << " FOIS" << std::endl;
    return partieGagne;
}

void pileFace::bataille() { //permet de jouer la partie
    //Initialise PV
    gamer->setPv(3);
    setOrdiPv(3);
    tourDeJeu();
    gagne();
}

void pileFace::tourDeJeu() { //bouble de jeu le temps que personne a perdu
    while ((gamer->getPv()>0) && (getOrdiPv()>0))
    {
        this->lancer();
    }
}

void pileFace::lancer() { //lancer de la pi�ce
    resRand = rand()%2;
    if (resRand==1)
    {
        //Impair = Face
        gamer->setPv(gamer->getPv()-1);
        setOrdiNbGagne(getOrdiNbGagne()+1);
        std::cout << "FACE ! ";
    }
    else
    {
        //Pair = Pile
        setOrdiPv(getOrdiPv()-1);
        gamer->setCompteurGagne(gamer->getCompteurGagne()+1);
        std::cout << "PILE ! ";
    }
}

void pileFace::gagne() //permet de savoir qui a gagn� la bataille.
{
    if (gamer->getPv()>0)
    {
        std::cout << "PILE A GAGNE : " << gamer->getCompteurGagne()<<std::endl;
    }
    else
    {
        std::cout << "FACE A GAGNE : " << getOrdiNbGagne()<<std::endl;
    }
}


